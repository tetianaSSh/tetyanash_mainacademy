FROM python:3.6
RUN mkdir /homework
WORKDIR /homework
COPY requirements.txt .
RUN pip install -r /requirements.txt
#створюємо каталог в homework
RUN mkdir -p /lesson5
COPY task_lesson5.py /lesson5
EXPOSE 80
CMD["python", "task_lesson5.py"]