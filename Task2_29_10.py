# Якщо записати числа від 1 до 5 англійськими словами (one, two, three, four, five),
# то використовується всього 3 + 3 + 5 + 4 + 4 = 19 букв.
# Скільки букв знадобиться для запису всіх чисел від 1 до 1000 (one thousand) включно?
# Примітка: Не враховувати прогалини і дефіси. Наприклад, число 342 (three hundred and forty-two)
# складається з 23 букв, число 115 (one hundred and fifteen) - з 20 букв.
# Використання "and" під час запису чисел відповідає правилам британського англійської.

def number_to_text(number):
    number_clovnik = {1: 'one', 2: 'two', 3: 'tree', 4: 'four', 5: 'five', 6: 'six', 7: 'seven', 8: 'eight', 9: 'nine',
                      10: 'ten', 11: 'eleven', 12: 'twelve', 13: 'thirteen', 14: 'fourteen',
                      15: 'fifteen', 16: 'sixteen', 17: 'seventeen', 18: 'eighteen', 19: 'nineteen', 20: 'twenty',
                      30: 'thirty', 40: 'forty', 50: 'fifty', 60: 'sixty', 70: 'seventy', 80: 'eighty', 90: 'ninety',
                      100: 'hundred', 1000: 'thousand'}
    chislo = int(number)
    number_text = ''
    if chislo < 20:
        number_text = number_clovnik.get(chislo)
    else:
        masiv = []
        rozryad = 0
        while chislo != 0:
            masiv.append(((chislo % 10) * 10 ** rozryad))
            # print(chislo % 10 * 10 ** rozryad)
            chislo = chislo // 10
            rozryad += 1
            i = 0
        while i < len(masiv) - 1:
            if i == 0 and masiv[i] != 0 and masiv[i + 1] != 0:
                number_text = '-' + number_clovnik.get(masiv[i]) + number_text
            elif i >= 2 and masiv[i] != 0:
                number_text = ' and ' + number_clovnik.get(masiv[i] // (10 ** i)) + ' ' + number_clovnik.get(
                    10 ** i) + number_text
            else:
                if masiv[i] != 0:
                    number_text = ' and ' + number_clovnik.get(masiv[i]) + number_text
            i += 1
        number_text = number_clovnik.get(masiv[i] // 10 ** i) + ' ' + number_clovnik.get(10 ** i) + number_text
    print(number_text)
    return number_text


def count_liter(number):
    number_text = number_to_text(number)
    i = 0
    count_bukv = 0
    while i < len(number_text):
        if not (number_text[i] in [' ', '-']):
            count_bukv += 1
        i += 1
    return count_bukv


number = input("Введіть ціле число від 1 до <10000:\n")
while number.isdigit() != False and int(number) < 1 or int(number) >= 10000:
    number = input("Будьте уважні! Введіть ціле число від 1 до <10000:\n")

print(count_liter(number), " букви знадобиться для запису англійською мовою числа ", number)
