# from math import fmod
import math


# Завдання 3 (29/10/2018)
# Написати функцію переводення число з десяткової системи числення в двійкову систему числення

# РЕАЛІЗАЦІЯ
# Функція convert_dec_to_binary1() видає результат у форматі 4 = 110, а
# функція convert_dec_to_binary2() видає результат у форматі 4 = ob110,
# проте обидві використовують різні підходи до виділення цілої і дробової частини числа для наступного
# їх переведення в двійкову систему числення.


def convert_dec_to_binary1(number_dec):
    number_dec = str(number_dec)
    if number_dec[0] == '-':
        if '.' in number_dec[1::]:
            if number_dec[1] != '.':
                cila_part = int(number_dec[1::].split('.')[0])
            else:
                cila_part = 0
            drob_part = int(number_dec[1::].split('.')[1]) / (10 ** len(number_dec[1::].split('.')[1]))
            sign = True
        else:
            cila_part = int(number_dec[1::])
            drob_part = 0
            sign = True
    else:
        if '.' in number_dec:
            if number_dec[0] != '.':
                cila_part = int(number_dec.split('.')[0])
            else:
                cila_part = 0
            drob_part = int(number_dec.split('.')[1]) / (10 ** len(number_dec.split('.')[1]))
            sign = False
        else:
            cila_part = int(number_dec)
            drob_part = 0
            sign = False
    # print("Ціла частина в 10-й системі числення: ", cila_part)
    # print("Дробова частина в 10-й системі числення: ", drob_part)
    if drob_part != .0:
        if sign == True:
            number_bin = '-' + convert_cila_part(cila_part) + convert_drob_part(drob_part)
        else:
            number_bin = convert_cila_part(cila_part) + convert_drob_part(drob_part)
    else:
        if sign == True:
            number_bin = '-' + convert_cila_part(cila_part)
        else:
            number_bin = convert_cila_part(cila_part)
    return number_bin


def convert_dec_to_binary2(number_dec):
    number_dec = str(number_dec)
    if number_dec[0] == '-':
        if '.' in number_dec[1::]:
            cila_part = int(math.modf(float(number_dec[1::]))[1])
            drob_part = float(number_dec[1::]) - cila_part
            sign = True
        else:
            cila_part = int(number_dec[1::])
            drob_part = .0
            sign = True
    else:
        if '.' in number_dec:
            cila_part = int(math.modf(float(number_dec))[1])
            drob_part = float(number_dec) - cila_part
            sign = False
        else:
            cila_part = int(number_dec)
            drob_part = .0
            sign = False
    # print("Ціла частина в 10-й системі числення: ", cila_part)
    # print("Дробова частина в 10-й системі числення: ", drob_part)
    if drob_part != .0:
        if sign == True:
            number_bin = '-0b' + convert_cila_part(cila_part) + convert_drob_part(drob_part)
        else:
            number_bin = '0b' + convert_cila_part(cila_part) + convert_drob_part(drob_part)
    else:
        if sign == True:
            number_bin = '-0b' + convert_cila_part(cila_part)
        else:
            number_bin = '0b' + convert_cila_part(cila_part)
    return number_bin

# Переведення у двійкову систему числення цілої частини десятковго числа
def convert_cila_part(cila_part):
    number_bin_cila = ''
    while (cila_part // 2) != 0:
        number_bin_cila = str(cila_part % 2) + number_bin_cila
        cila_part = cila_part // 2
    number_bin_cila = str(cila_part % 2) + number_bin_cila
    # print("ціла частина в 2-й системі числення:: ", number_bin_cila)
    return number_bin_cila

# Переведення у двійкову систему числення дробової частини десятковго числа
def convert_drob_part(drob_part):
    number_bin_drob = ''
    rozryad = 0  # кількість знаків після коми
    number_bin_drob = number_bin_drob + '.'
    while drob_part != .0 and rozryad < 10:
        rozryad += 1
        number_bin_drob = number_bin_drob + str(math.trunc(drob_part * 2))
        drob_part = drob_part * 2 - math.trunc(drob_part * 2)
    # print("дробова частина в 2-й системі числення:: ", number_bin_drob)
    return number_bin_drob

# Визначення чи введене значення є числом: ціле чи дійсне
def is_chislo(number):
    if number[0] == '-':
        i = 1
        flag = True
        kilk_toch = 0
        kilk_cifr = 0
        while i < len(number[1::]):
            if number[i] in ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']:
                i += 1
                kilk_cifr += 1
            elif number[i] == '.' and kilk_toch <= 1:
                i += 1
                kilk_toch += 1
            else:
                flag = False
                break
    else:
        i = 1
        flag = True
        kilk_toch = 0
        kilk_cifr = 0
        while i < len(number):
            if number[i] in ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']:
                i += 1
                kilk_cifr += 1
            else:
                if number[i] == '.' and kilk_toch <= 1 and number[-1] != '.':
                    i += 1
                    kilk_toch += 1
                else:
                    flag = False
                    break
    print("flag", flag)
    return flag


print("234 =", convert_dec_to_binary1(234))
print("234 =", convert_dec_to_binary2(234))
print("234.56 =", convert_dec_to_binary1(234.56))
print("234.56 =", convert_dec_to_binary2(234.56))
print("-234.56 =", convert_dec_to_binary1(-234.56))
print("-234.56 =", convert_dec_to_binary2(-234.56))
print(".56 =", convert_dec_to_binary1(.56))
print(".56 =", convert_dec_to_binary2(.56))
print("0.56 =", convert_dec_to_binary1(0.56))
print("0.56 =", convert_dec_to_binary2(0.56))

# number - не тільки ціле число, забула просто виправити і забрати слово "ціле"
number = input("Введіть десяткове число, у форматі ціле чи дійсне):\n")
flag = is_chislo(number)
while flag != True:
    number = input("Будьте уважні! Введіть число:\n")
    flag = is_chislo(number)

# Використання функції convert_dec_to_binary1
print("ЧИСЛО В ДВІЙКОВІЙ СИСТЕМІ ЧИСЛЕННЯ: ", convert_dec_to_binary1(number))
print('-' * 20)
# Використання функції convert_dec_to_binary1
print("ЧИСЛО В ДВІЙКОВІЙ СИСТЕМІ ЧИСЛЕННЯ: ", convert_dec_to_binary2(number))
